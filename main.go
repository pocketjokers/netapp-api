package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	cmd "gitlab.com/pocketjokers/netapp-api/commands"

	log "github.com/sirupsen/logrus"
)

// Setup listen address and port
var (
	// TODO allow port override instead of setting statically
	exposedPort   string = ":8888"
	rootdir       string = "/opt/netapp/"
	listenAddress        = flag.String("web.listen-address", exposedPort,
		"Address on which to expose metrics and web interface.")
)

func retrieveHost() string {
	b, err := ioutil.ReadFile(rootdir + "containerhost.txt") // read container ip from file
	if err != nil {
		fmt.Print(err)
	}
	containerIP := string(b) // convert content to a 'string'

	return containerIP
}

func main() {
	// retrieve container ip
	containerIP := retrieveHost()
	containerIP = strings.TrimSuffix(containerIP, "\n")

	http.HandleFunc("/upload-archive", cmd.ZipHandler)

	http.HandleFunc("/read-archive", func(res http.ResponseWriter, req *http.Request) {
		cmd.ReadArchive(res, req)
	})

	//---------------------------------------------------------------------
	// Setup HTTP Client
	//---------------------------------------------------------------------
	log.Info("---------------------------------------------------------------------")
	log.Info("|| netapp-api listening on " + containerIP + exposedPort)
	log.Info("---------------------------------------------------------------------")

	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		log.Fatalf("Unable to setup HTTP server: %v", err)
	}
}
