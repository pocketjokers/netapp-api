#!/bin/bash

# Uncomment set -x to enable debug view
#set -x

# Set up locale
if [ -f /etc/profile.d/lang.sh ];  then
    . /etc/profile.d/lang.sh
fi

###############################################################################
# Variables (defaults)
###############################################################################

ROOTDIR="/opt/netapp"
_ARCHIVE_ID=""
_LOGDIR=""
_DEPLOY_DIR=$ROOTDIR/$_ARCHIVE_ID

###############################################################################
# Functions
###############################################################################

function echo_line {
    line_out="$(date) - $1"
    printf "%s\n" "$line_out"
}

function container_host {
    # retrieving container ip from host file
    export HOST=$(tail -n 1 /etc/hosts)
    echo_line "HOST: ${HOST}"
    SUBSTR=$(echo $HOST | cut -d' ' -f 1)
    echo_line $SUBSTR

    # writing ip to file on disk
    echo $SUBSTR > containerhost.txt
}

container_host

###############################################################################
# netapp-api
###############################################################################

PROCESS_CONTROLLER=./netapp-api

echo_line "checking to see if netapp-api exists"
    if [ -f ${PROCESS_CONTROLLER} ]; then
        echo_line "netapp-api found!"
        echo_line "running netap-api..."
        $PROCESS_CONTROLLER
    else
	rc = $?
        echo_line "netapp-api does NOT exist"
	exit $rc
    fi

exit 0
