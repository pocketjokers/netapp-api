module gitlab.com/pocketjokers/netapp-api

go 1.17

require github.com/sirupsen/logrus v1.8.1

require (
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/sys v0.0.0-20200803210538-64077c9b5642 // indirect
)
