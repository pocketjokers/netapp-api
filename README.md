# Netapp Api

## Overview

This is a containerized HTTP API, with a golang module handling REST calls. It takes archives as input, writes information to the container in csv files, and can be retrieved later.

## Installation

There are two ways to run this application:

1. Clone the git repo locally, and then build and run the container:

```
cd netapp-api
docker build -t netapp-api . && docker run netapp-api
```

2. Pull the container directly from docker hub:

```
docker pull kjferris87/netapp-api:1.00
docker run <IMAGE ID>
```

The running container should look something like this, and will also list the addreess to make the REST calls:

```
Sat Dec  4 03:48:04 UTC 2021 - checking to see if netapp-api exists
Sat Dec  4 03:48:04 UTC 2021 - netapp-api found!
Sat Dec  4 03:48:04 UTC 2021 - running netap-api...
time="2021-12-04T03:48:04Z" level=info msg=---------------------------------------------------------------------
time="2021-12-04T03:48:04Z" level=info msg="|| netapp-api listening on 172.17.0.2:8888"
time="2021-12-04T03:48:04Z" level=info msg=---------------------------------------------------------------------
```

## Usage

This application can be used to both upload and read archives over REST with the following endpoints:

Upload archive:
`curl {{ IP-ADDRESS }}:8888/upload-archive?archive-tag={{ UNIQUE-TAG }} -v -H "Content-Type:application/zip" --data-binary @/{{ PATH-TO-ARCHIVE}}`
example:
`curl 172.17.0.2:8888/upload-archive?archive-tag=kyferr -v -H "Content-Type:application/zip" --data-binary @NetApp_CND_exercise.zip`

Read archive:
`curl {{ IP-ADDRESS }}:8888/read-archive?archive-tag={{ UNIQUE-TAG }}`
example:
`curl 172.17.0.2:8888/read-archive?archive-tag=kyferr`

As previously mentioned, the running container will show the correct ip address to connect to the client externally.

## Support
https://gitlab.com/pocketjokers/netapp-api/

## Roadmap
Add unit testing
Make packages more granular
Add more REST functionality
Allow overiding of exposed port

## Authors and acknowledgment
Kyle Ferris
