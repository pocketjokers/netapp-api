FROM golang:1.17-alpine as builder

# set working directory
WORKDIR /opt/netapp

# copy files
COPY . /opt/netapp

# build go module
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" .

USER root

# expose port in container
EXPOSE 8888

RUN chmod +x /opt/netapp/entrypoint.sh

# container entrypoint script
ENTRYPOINT ["sh","./entrypoint.sh"]
