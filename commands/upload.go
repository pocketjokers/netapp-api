package commands

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

func ZipHandler(rw http.ResponseWriter, r *http.Request) {
	requestLogger := log.WithFields(log.Fields{"source": "netapp-api", "version": 1})
	fmt.Println("endpoint hit")
	log.Info("endpoint hit")
	requestLogger.Info("GET client request successful 1")
	zipName := "ZipTest.zip"
	rw.Header().Set("Content-Type", "application/zip")
	rw.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", zipName))
	err := getZip(rw)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()
	requestLogger.Info()
	buf, err := io.ReadAll(r.Body)

	archiveTag := r.URL.Query().Get("archive-tag")
	requestLogger.Info("archive-tag: " + archiveTag)

	zipReader, err := zip.NewReader(bytes.NewReader(buf), int64(len(buf)))
	if err != nil {
		log.Fatal(err)
	}
	requestLogger.Info(zipReader)
	rootdir := "/opt/netapp/"

	archiveDirectory := rootdir + "/" + archiveTag
	// create a directory with the archive-tag
	err3 := os.Mkdir(archiveDirectory, 0755)
	check(err3)

	// Read all the files from zip archive and write into the container
	// this information could also be entered in a database or key-value store
	for _, zipFile := range zipReader.File {
		fileName := zipFile.Name
		fileName = strings.Replace(fileName, "/", ".", -1)
		fileName = strings.Replace(fileName, "..", ".", -1)
		nameFile, err := os.Create(archiveDirectory + "/" + fileName + ".csv")
		defer nameFile.Close()
		fmt.Println("Reading file:", zipFile.Name)
		nameFile.WriteString("Name," + zipFile.Name + "\n")

		fmt.Println("Reading file CRC32:", zipFile.CRC32)
		var crc32Str = strconv.FormatUint(uint64(zipFile.CRC32), 10)
		nameFile.WriteString("CRC32," + crc32Str + "\n")

		fmt.Println("Reading file comment:", zipFile.Comment)
		nameFile.WriteString("Comment," + zipFile.Comment + "\n")

		fmt.Println("Reading file creator version:", zipFile.CreatorVersion)
		var creatorVersionStr = strconv.FormatUint(uint64(zipFile.CreatorVersion), 10)
		nameFile.WriteString("CreatorVersion," + creatorVersionStr + "\n")

		fmt.Println("Reading file external attributes:", zipFile.ExternalAttrs)
		var externalAttrsStr = strconv.FormatUint(uint64(zipFile.CreatorVersion), 10)
		nameFile.WriteString("ExternalAttrs," + externalAttrsStr + "\n")

		fmt.Println("Reading file method:", zipFile.Method)
		var methodStr = strconv.FormatUint(uint64(zipFile.Method), 10)
		nameFile.WriteString("Method," + methodStr + "\n")

		fmt.Println("Reading file info is dir:", zipFile.FileInfo().IsDir())
		var isDirStr = strconv.FormatBool(zipFile.FileInfo().IsDir())
		nameFile.WriteString("isDir," + isDirStr + "\n")

		fmt.Println("Reading file info mod time:", zipFile.FileInfo().ModTime())
		nameFile.WriteString("ModTime," + zipFile.FileInfo().ModTime().String() + "\n")

		fmt.Println("Reading file info mode:", zipFile.FileInfo().Mode())
		nameFile.WriteString("Mode," + zipFile.FileInfo().Mode().String() + "\n")

		fmt.Println("Reading file modified:", zipFile.Modified)
		nameFile.WriteString("Modified," + zipFile.Modified.String() + "\n")

		fmt.Println("Reading file reader version:", zipFile.ReaderVersion)
		var readerVersionStr = strconv.FormatUint(uint64(zipFile.ReaderVersion), 10)
		nameFile.WriteString("ReaderVersion," + readerVersionStr + "\n")

		fmt.Println("Reading file uncompressed size (64):", zipFile.UncompressedSize64)
		var uncompressedSize64Str = strconv.FormatUint(uint64(zipFile.UncompressedSize64), 10)
		nameFile.WriteString("UncompressedSize64," + uncompressedSize64Str + "\n")

		unzippedFileBytes, err := readZipFile(zipFile)
		if err != nil {
			log.Println(err)
			continue
		}

		_ = unzippedFileBytes // this is unzipped file bytes
	}

}

func readZipFile(zf *zip.File) ([]byte, error) {
	f, err := zf.Open()
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ioutil.ReadAll(f)
}

func getZip(w io.Writer) error {
	// zip writer
	zipW := zip.NewWriter(w)
	defer zipW.Close()

	for i := 0; i < 5; i++ {
		f, err := zipW.Create(strconv.Itoa(i) + ".txt")
		if err != nil {
			return err
		}
		_, err = f.Write([]byte(fmt.Sprintf("Hello file %d", i)))
		if err != nil {
			return err
		}
	}
	return nil
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
