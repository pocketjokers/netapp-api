package commands

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func ReadArchive(rw http.ResponseWriter, r *http.Request) {
	//-------------------------------------------------------
	// Read previously uploaded archives based on archive tag
	//-------------------------------------------------------

	// retriveve parameter from url
	archiveTag := r.URL.Query().Get("archive-tag")
	rootdir := "/opt/netapp/"

	rw.Write([]byte("archive-tag: " + archiveTag + "\n"))

	archiveDirectory := rootdir + "/" + archiveTag

	files, err := ioutil.ReadDir(archiveDirectory + "/")
	if err != nil {
		rw.Write([]byte("couldn't find archive-tag: " + archiveTag))
	}

	// range over files in the archive directory
	for _, file := range files {
		fmt.Println(file.Name(), file.IsDir())
		rw.Write([]byte(file.Name()))
		rw.Write([]byte("\n"))

		// return information about each file to client
		file, err := os.Open(archiveDirectory + "/" + file.Name())
		if err != nil {
			rw.Write([]byte("couldn't find file: " + file.Name()))
			rw.Write([]byte("\n"))
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			rw.Write([]byte("      "))
			rw.Write([]byte(scanner.Bytes()))
			rw.Write([]byte("\n"))
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}

	rw.Write([]byte("\n"))
	return
}
